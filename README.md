Module 'Git'
==
This module is focused on using [Git](https://git-scm.com/) to manage and track the history of your files.

## Prerequisites
  - Basic introduction to [Git] [url].
  [url]: https://guides.github.com/introduction/git-handbook/

## Chapter 01: Getting Started
  - Objectives:
    - [Cloning/getting a repository to your local machine.](https://git-scm.com/docs)
    - [Checking the status of your files](https://git-scm.com/docs/git-status)  
    - [Adding changes to commit](https://git-scm.com/docs/git-add)
    - [Committing your changes](https://git-scm.com/docs/git-commit)
    - [Pushing your files to your remote repository](https://git-scm.com/docs/git-push)
    - [Pulling the latest version of the remote into your local repository](https://git-scm.com/docs/git-pull)  

  - Excercises:
    - Clone this repository to your local machine.
    - Open a text editor or IDE of choice and open the `hello-world-app`
    - Navigate to `src/app/app.component.html`
    - Change the text to "Hello, *your name*"
    - Check your Changes (status)
        - [Read about the different states of your files](https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository)
    - Commit your changes with message "My first commit!"
        - [Also read about adding extra changes to a previous commit](https://git-scm.com/docs/git-commit#git-commit---amend)
    - Push your changes.
    - Go to your remote repository in your browser of choice, make a change to `src/app/app.component.html` and commit it (inside the browser)
    - Pull the latest version into your local repository

## Chapter 02: Working with branches
  - Objectives:
    - [Understanding the basics of branches in Git and creating a branch](https://git-scm.com/book/en/v2/Git-Branching-Branches-in-a-Nutshell)
    - [Learn how to switch branches](https://git-scm.com/docs/git-checkout)
    - [Using tags to indicate important points in the history of the repository/branch](https://git-scm.com/docs/git-tag)
    - [Merging your branch with the master branch](https://git-scm.com/docs/git-merge)

  - Excercises:
    - Create a branch called "Development"
    - Switch to branch "Development"
    - In `src/app/app.component.html` change the text to "Hello from my development branch, version 0.1"
    - Commit the changes
    - Create an annotated tag marking version `0.1` for your branch.
    - Next, change the text to "Hello from my development branch, version 0.2"
    - Create an annotated tag marking version `0.2` for your branch.
    - Commit the changes
    - Next, change the text to "Hello from my development branch, current version"
    - Commit the changes
    - Checkout version `0.1` and `0.2` to see how your files were in the "past"
    - Switch back to your development branch and merge it with the master branch
        - [Read about rebasing](https://git-scm.com/docs/git-rebase)
        - [Read about merging vs rebasing](https://www.atlassian.com/git/tutorials/merging-vs-rebasing)

## Chapter 03: Undoing Changes
  - Objectives:
    - [Resetting the HEAD to a specific state](https://git-scm.com/docs/git-reset)
        - [More detailed explanation of the git-reset command](https://www.atlassian.com/git/tutorials/undoing-changes/git-reset)
    - [Reverting existing commits](https://git-scm.com/docs/git-revert)
        - [More detailed explanation of the git-revert command](https://www.atlassian.com/git/tutorials/undoing-changes/git-revert)

    - [Stashing changes for later use](https://git-scm.com/docs/git-stash)
        - [More detailed explanation of the git stash command](https://www.atlassian.com/git/tutorials/saving-changes/git-stash)

  - Excercises
    - Make some changes and commit them
    - Undo the commit with either `git-revert` or `git-reset`
        - [The difference between git-reset and git-revert](https://stackoverflow.com/questions/8358035/whats-the-difference-between-git-revert-checkout-and-reset)
    - Make some changes and stash them
    - Reapply the stashed changes

## Chapter 04: Working with Gitflow
  - Objectives:
    - Learn about Git workflows
    - Learn about the popular workflow: Gitflow

  - Excercises:
    - [Read the Gitflow tutorial by Atlassian](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow)
